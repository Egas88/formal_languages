import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String sentence = scanner.nextLine();
        RegChecker checker = new RegChecker();
        if (checker.check(sentence)) {
            System.out.println("Success");
        } else {
            System.out.println("Неудачно");
        }
    }
}
