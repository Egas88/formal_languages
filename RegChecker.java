import java.util.Collections;
import java.util.Stack;

public class RegChecker {

    private enum State{
        s1, s2, s3, s4
    }
    public boolean check(String s){
        State currentState = State.s1;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (currentState) {
                case s1:
                    if (c == 'a') {
                        currentState = State.s2;
                    } else {
                        return false;
                    }
                    break;
                case s2:
                    if (c == 'b') {
                        currentState = State.s3;
                    } else if (c != 'a'){
                        return false;
                    }
                    break;
                case s3:
                    if (c == 'b' || c == 'c') {
                        currentState = State.s4;
                    } else if (c == 'a'){
                        currentState = State.s2;
                    } else {
                        return false;
                    }
                    break;
                case s4:
                    if (!(c == 'b' || c == 'c')) {
                        return false;
                    }
                    break;
            }
        }
        return currentState == State.s4;
    }

}
